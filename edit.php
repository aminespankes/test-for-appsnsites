<?php
require 'db.php';
$id = $_GET['id'];
$sql = 'SELECT * FROM inscri_test WHERE id=:id';
$statement = $connection->prepare($sql);
$statement->execute([':id' => $id ]);
$person = $statement->fetch(PDO::FETCH_OBJ);
if (isset ($_POST['nom'])  && isset($_POST['email']) ) {
  $nom = $_POST['nom'];
    $date = $_POST['date'];
    $adress = $_POST['adress'];
    $site = $_POST['site'];
    
  $email = $_POST['email'];
  $sql = 'UPDATE inscri_test SET nom=:nom, date=:date,adress=:adress,site=:site,email=:email WHERE id=:id';
  $statement = $connection->prepare($sql);
  if ($statement->execute([':nom' => $nom, ':date' => $date,':adress' => $adress,':site' => $site,':email' => $email, ':id' => $id])) {
    header("Location: /test-inscri/");
  }



}


 ?>
<?php require 'header.php'; ?>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Update person</h2>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
      <form method="post">
        <div class="form-group">
          <label for="name">Nom</label>
          <input value="<?= $person->nom; ?>" type="text" name="nom" id="nom" class="form-control">
        </div>
          <div class="form-group">
          <label for="date">Date de naissance</label>
          <input type="date" value="<?= $person->date; ?>" name="date" id="date" class="form-control">
        </div>
          <div class="form-group">
          <label for="email">adresse</label>
          <input type="adress" value="<?= $person->adress; ?>" name="adress" id="adress" class="form-control">
        </div>
          <div class="form-group">
          <label for="email">Site Internet</label>
          <input type="site" value="<?= $person->site; ?>" name="site" id="site" class="form-control">
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" value="<?= $person->email; ?>" name="email" id="email" class="form-control">
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-info">Update person</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php require 'footer.php'; ?>
